//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"", "music", 0, 5},
//	{"", "weather", 21600, 0},
	{"", "memory", 1, 0},
//	{"", "disk / 💾", 1, 0},
	{"", "disk /home 🏠", 1, 0},
//	{"", "disk /media 📼", 1, 0},
	{"", "updates", 0, 7},
	{"", "volume", 0, 10},
	{"", "internet", 10, 0},
	{"", "cal-widget", 60, 0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = '|';
